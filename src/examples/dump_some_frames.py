import sys
import Image
from oggplay import *
from xml.dom.ext.reader import Sax2
from xml import xpath

video_name = sys.argv[1]
timelist = sys.argv[2]
outname = sys.argv[3]
f = open(timelist)

reader = Sax2.Reader()
doc = reader.fromStream(f)
times = xpath.Evaluate('descendant::clip/@start', doc.documentElement)
times = map(lambda a: a.nodeValue, times)
f.close()

def string_to_ms(time):
  stime = time.split(":")
  out = 0
  mul = 1
  while len(stime) > 0:
    out += mul * float(stime[-1])
    mul *= 60
    del stime[-1]
  return (int(out * 1000 + 500), time)

times = map(string_to_ms, times)

if video_name[:7] == "http://":
  reader = oggplay_tcp_reader_new(video_name, None, 80)
else:
  reader = oggplay_file_reader_new(video_name)

player = oggplay_open_with_reader(reader)

for i in range(oggplay_get_num_tracks(player)):
  global video
  if oggplay_get_track_type(player, i) == OGGZ_CONTENT_THEORA:
    oggplay_set_callback_num_frames(player, i, 1)
    video = i
  oggplay_set_track_active(player, i)

pos = 0

def data_callback(player, track_info):
  global pos
  headers = oggplay_callback_info_get_headers(track_info[video])
  time = oggplay_callback_info_get_presentation_time(headers[0])
  if time >= times[pos][0]:  
    rgba = oggplay_generate_frame(player, video, headers[0])
    image = Image.fromstring("RGBA", rgba[1], rgba[0])
    fname = outname + ".png_t=" + times[pos][1]
    pos += 1
    print "saving", fname
    image.save(fname, "PNG")
    if pos == len(times):
      return -1
  return 0

if len(times) == 0:
  exit(0)
oggplay_set_data_pycallback(player, data_callback)
oggplay_start_decoding(player)
