#include "config.h"

#include <oggplay/oggplay.h>
#include <stdio.h>
#include <stdlib.h>

int
main (int argc, char * argv[]) {

  OggPlay       * player;
  OggPlayReader * reader;
  int             i;

  if (argc < 2) {
    printf ("please provide a filename\n");
    exit (1);
  }
  
  reader = oggplay_file_reader_new(argv[1]);

  player = oggplay_open_with_reader(reader);

  if (player == NULL) {
    printf ("could not initialise oggplay with this file\n");
    exit (1);
  }

  for (i = 0; i < oggplay_get_num_tracks (player); i++) {
    printf("Track %d is of type %s\n", i, 
                    oggplay_get_track_typename (player, i));
  }
 
  return 0; 
}
