#include "config.h"

#include <oggplay/oggplay.h>
#include <stdio.h>
#include <stdlib.h>

#include "oggplay_tests.h"

int
main (int argc, char * argv[]) {
/*
  OggPlayReader * reader;
  OggPlay       * player;
  OggPlayErrorCode err;
*/

  INFO ("Creating new OggPlayReader");

/* XXX: Need a file to instantiate OggPlay (oggplay_init() is not exported)
  INFO ("Creating new OggPlay instance");
  reader = oggplay_file_reader_new("/dev/full");
  player = oggplay_open_with_reader(reader);

  if (player == NULL) {
    FAIL ("Could not initialise OggPlay\n");
    exit (1);
  }

  INFO ("Closing OggPlay instance");
  err = oggplay_close (player);

  printf ("err: %d\n", err);
*/

/* XXX: Need a way to delete an OggPlayReader
  INFO ("Deleting OggPlayReader");
  oggplay_reader_delete (reader);
*/

  exit (0); 
}
